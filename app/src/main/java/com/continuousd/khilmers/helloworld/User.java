package com.continuousd.khilmers.helloworld;

public class User {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        name = value;
    }
}
