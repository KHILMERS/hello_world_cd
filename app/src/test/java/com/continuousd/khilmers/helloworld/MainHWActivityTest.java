package com.continuousd.khilmers.helloworld;

import android.content.Intent;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenu;
import org.robolectric.fakes.RoboMenuItem;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainHWActivityTest {

    @Test
    public void mailButtonTest() {
        MainHWActivity activity = Robolectric.setupActivity(MainHWActivity.class);
        activity.findViewById(R.id.fab).performClick();

        Intent expectedIntent = new Intent(activity, LoginActivity.class);
        assertThat(shadowOf(activity).getNextStartedActivity().getClass()).isEqualTo(expectedIntent.getClass());
    }

    @Test
    public void actionBarSettingsTest() {
        RoboMenuItem item = new RoboMenuItem(R.id.action_settings);
        MainHWActivity activity = Robolectric.setupActivity(MainHWActivity.class);
        RoboMenu menu = new RoboMenu(activity);
        assertThat(activity.onCreateOptionsMenu(menu)).isEqualTo(true);
        assertThat(activity.onOptionsItemSelected(item)).isEqualTo(true);
    }
}
