package com.continuousd.khilmers.helloworld;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
public class UserTest {

    @Test
    public void UserNameTest() {
        User u = new User();

        u.setName("pelle");

        Assert.assertEquals("pelle", u.getName());
    }

    @Test
    public void WrongUserNameTest() {
        User u = new User();

        u.setName("rolf");

        Assert.assertNotSame("pelle", u.getName());
    }

    @Test
    public void LongUserNameTest() {
        User u = new User();

        u.setName("Hubert Blaine Wolfeschlegelsteinhausenbergerdorff");

        Assert.assertEquals("Hubert Blaine Wolfeschlegelsteinhausenbergerdorff", u.getName());
    }

}
