package com.continuousd.khilmers.helloworld;

import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LoginTest {

    @Test
    public void correctLoginTest() {
        LoginActivity activity = Robolectric.setupActivity(LoginActivity.class);
        TextView email  = (TextView) activity.findViewById(R.id.email);
        TextView pass   = (TextView) activity.findViewById(R.id.password);
        email.setText("foo@example.com");
        pass.setText("hello");

        activity.findViewById(R.id.email_sign_in_button).performClick();

    }
}